#include "myheader.h"

// Swap rows i and k of a matrix A
// Note that due to the reference, both dimensions are preserved for
// built-in arrays
//template<typename MatrixType>
 void swap_rows(blaze::DynamicMatrix<int>& A, int i, int j)
{
  blaze::DynamicVector<int> temporary;
  for(int l = 0; l < blaze::columns(A); ++l){
      temporary[l] = A(i,l);
  }
  blaze::row(A,i) = blaze::row(A,j);
  for(int l = 0; l < blaze::columns(A); ++l){
      A(j,l) = temporary[l];
  }
  return;
}
 
// divide row i of matrix A by v
//template<typename MatrixType>
 void divide_row(blaze::DynamicMatrix<int>& A, int i, int v)
{
     for(int l = 0; l < blaze::columns(A); ++i){
         A(i,l) /= v;
     }
     return;
}
 
// in matrix A, add v times row k to row i
//template<typename MatrixType>
 void add_multiple_row(blaze::DynamicMatrix<int>& A, int i, int j, int v)
{
     for(int l = 0; l < blaze::columns(A); ++l){
         A(i,l) += (v * A(j,l));
     }
     return;
}
 
// convert A to reduced row echelon form
//template<typename MatrixType>
 void to_reduced_row_echelon_form(blaze::DynamicMatrix<int>& A, blaze::DynamicMatrix<int>& X)
{
  int max_rows = blaze::rows(A) - 1;
  int max_cols = blaze::columns(A) - 1;
  int lead = 0;
 
  for (int row = 0; row <= max_rows; ++row)
  {
    if (lead > max_cols){
      return;
    }
    int i = row;
    while (0 == A(i,lead))
    {
      ++i;
      if (i > max_rows)
      {
        i = row;
        ++lead;
        if (lead > max_cols)
          return;
      }
    }
    swap_rows(A, i, row);
    swap_rows(X, i, row);
    divide_row(X, row, A(row,lead));
    divide_row(A, row, A(row,lead));
    for (i = 0; i <= max_rows; ++i)
    {
      if (i != row){
        add_multiple_row(X, i, row, -A(i,lead));
        add_multiple_row(A, i, row, -A(i,lead));
      }
    }
  }
  return;
}
 
void to_reduced_row_echelon_form_without_X(blaze::DynamicMatrix<int>& A)
{
  int max_rows = blaze::rows(A) - 1;
  int max_cols = blaze::columns(A) - 1;
  int lead = 0;
 
  for (int row = 0; row <= max_rows; ++row)
  {
    if (lead > max_cols){
      return;
    }
    int i = row;
    while (A(i,lead) == 0)
    {
      ++i;
      if (i > max_rows)
      {
        i = row;
        ++lead;
        if (lead > max_cols)
          return;
      }
    }
    swap_rows(A, i, row);
    divide_row(A, row, A(row,lead));
    for (i = 0; i <= max_rows; ++i)
    {
      if (i != row){
        add_multiple_row(A, i, row, -A(i,lead));
      }
    }
  }
  return;
}