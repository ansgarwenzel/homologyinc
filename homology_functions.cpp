//This file includes the functions required to calculate the homology of a
//(bi)rack, based on boundary matrices.
#include "myheader.h"


int matrix_rank(blaze::DynamicMatrix<int>& A){
    int rank = 0;
    while(0 != (blaze::row(A,rank).nonZeros())){
        rank++;
    }
    return(rank);
}

void check_for_gcd(int m, int n, int *pt_matrix[]){
    int gcd_val;
    for(int i = 0; i < m; ++i){
        gcd_val = boost::math::gcd(*(pt_matrix[i]),*(pt_matrix[i] + 1));
        if(1 == gcd_val){
            continue;
        }
        for(int j = 2;j < n; ++j){
            gcd_val = boost::math::gcd(gcd_val, *(pt_matrix[i] + j));
            if(1 == gcd_val){
                break;
            }
        }
        if(1 <= std::abs(gcd_val)){
            for(int j = 0; j < n; ++j){
                *(pt_matrix[i] + j) = *(pt_matrix[i] + j) / gcd_val;
            }
        }
    }
    
    for(int i = 0; i < m; ++i){
        if(0>*(pt_matrix[i] + i)){
            for(int j = i; j < n; ++j){
                *(pt_matrix[i] + j) *= -1;
            }
        }
    }
    return;
}

int diag_to_smith(blaze::DynamicMatrix<int>& A){
    
    int minimum;
    minimum = std::min(blaze::columns(A),blaze::rows(A));
    for(int m = 0; m < (minimum - 2); ++m){
        for(int l = (minimum - 2); l >= m; --l){
            if(0!=A(l+1,l+1)){
                if(0!=(A(l,l)%A(l+1,l+1))){ //changed 0== to 0!=. Check!
                   int g = A(l,l) * A(l+1,l+1);
                   A(l,l) = boost::math::gcd(A(l,l),A(l+1,l+1));
                   A(l+1,l+1) = g / A(l,l);
                }
            }
        }
    }
    for(int l = 0; l < minimum; ++l){
        if(0!=A(l,l)){
            A(l,l) = std::abs(A(l,l));
        }
    }
    return(0);
}

int smith_normal_form(blaze::DynamicMatrix<int>& A){
    int i = 0;
    while(check_diagonal(A)){
        HNF(A);
        A = blaze::trans(A);
        HNF(A);
        A = blaze::trans(A);
        i++;
        if(i > 10){
            std::cout << "argh!\n";
            break;
        }
    }
    //std::cout << A << "\n";
    diag_to_smith(A);
    return(0);
}

bool check_diagonal(blaze::DynamicMatrix<int>& A){
    int total_nonzero = A.nonZeros();
    int diag_nonzero, min_row_col = std::min(blaze::columns(A),blaze::rows(A));
    blaze::DynamicVector<int> diag_A(min_row_col);
    for(int i = 0; i < min_row_col;++i){
        diag_A[i] = A(i,i);
    }
    diag_nonzero = diag_A.nonZeros();
    if(diag_nonzero == total_nonzero){
        return(false);
    }
    return(true);
}

void HNF(blaze::DynamicMatrix<int>& A){

    for(int t = 0; t < blaze::columns(A); ++t){
        int r = -1;
        for(int s = 0; s < blaze::rows(A); ++s){
            if((0!=A(s,r+1))||(0!=A(s,t))){ 
                r++;
                if(t==r){
                    if(0 > A(s,t)){
                        blaze::column(A,t) = -blaze::column(A,t);
                    } 
                } else{
                        row_gcd(A,s,r,t);
                    }
                for(int l = 0; l < (r-1); ++l){
                    blaze::column(A,l) = blaze::column(A,l) - (psi(A(s,l),A(s,r)) * blaze::column(A,r));
                }
                if(t==r){
                    break;
                }
            }
        }
    }
    return;
}


int psi(int a, int b){
    if(0 == b){
        return(a);
    } else{
        return(a - (std::floor(a/b) * b));
    }
}

void row_gcd(blaze::DynamicMatrix<int>& A, int i, int j, int l){
    if((0!=A(i,j))||(0!=A(i,l))){
        int duv[3];
        extendedEuclid(A(i,j),A(i,l),&duv[0]);
        blaze::DynamicMatrix<int> B(blaze::rows(A),2), mult_matrix(2,2);
        blaze::column(B,0) = blaze::column(A,j);
        blaze::column(B,1) = blaze::column(A,l);
        mult_matrix(0,0) = duv[1];
        mult_matrix(1,0) = duv[2];
        mult_matrix(0,1) = -(A(i,l) / duv[0]);
        mult_matrix(1,1) = A(i,j) / duv[0];
        B = B * mult_matrix;
        blaze::column(A,j) = blaze::column(B,0);
        blaze::column(A,l) = blaze::column(B,1);
    }
    return;
}

void extendedEuclid(int a, int b, int *results){            //this seems to work.
    int x = 1, y = 0, q, r, m, n;
    *(results + 1) = 0;
    *(results + 2) = 1;
    while(a != 0) {
        q = b / a;
        r = b % a;
        m = *(results + 1) - q * x;
        n = *(results + 2) - q * y;
        *(results + 1) = x;
        *(results + 2) = y;
        x = m, y = n;
        b = a, a = r;
    }
    *results = b;
}


void matrix_inverse(blaze::DynamicMatrix<int>& M, blaze::DynamicMatrix<int>& N){
    int m = blaze::rows(M), n = blaze::columns(M);
    gsl_matrix *V = gsl_matrix_alloc (n, n);
    gsl_matrix *A = gsl_matrix_alloc(m, n);
    gsl_vector *Sigma = gsl_vector_alloc(n);
    gsl_vector *work = gsl_vector_alloc(n);
    for(int i = 0; i < m; ++i){
        for(int j = 0; j < n; ++j){
            gsl_matrix_set(A, i, j, M(i, j));
        }
    }
      
    gsl_linalg_SV_decomp(A, V, Sigma, work);
    gsl_vector_free(work);

    blaze::DynamicMatrix<double> Sigma_b(n,n), U_b(m,n), V_b(n,n), N_b(blaze::rows(N),blaze::columns(N)); 
    Sigma_b = 0.;
    double sigma_value;
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
            V_b(i, j) = gsl_matrix_get(V, i, j);
        }
        for(int j = 0; j < m; ++j){
            U_b(j, i) = gsl_matrix_get(A, j, i);
        }
        sigma_value = gsl_vector_get(Sigma, i);
        if(0!=sigma_value){
            Sigma_b(i, i) = 1. / sigma_value;
        }
    }//check here again.
    gsl_matrix_free(V);
    gsl_matrix_free(A);
    gsl_vector_free(Sigma);
    for(int i = 0; i < blaze::rows(N);++i){
        for(int j = 0; j < blaze::columns(N);++j){
            N_b(i,j) = (double) N(i,j);
        }
    }
    V_b = V_b * Sigma_b * blaze::trans(U_b);
    
    M = N_b * (blaze::trans(V_b));
    
    return;
}