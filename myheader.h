/* 
 * File:   myheader.h
 * Author: Lord of dark Noses
 *
 * Created on 18. Mai 2014, 11:50
 */

#ifndef MYHEADER_H
#define	MYHEADER_H
#endif	/* MYHEADER_H */

#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <math.h>
#include <blaze/Math.h>
#include <cstddef>
#include <cassert>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <boost/math/common_factor.hpp>
#include <cstring>


using namespace std;

//functions by other people, but changed
/*void to_reduced_row_echelon_form(blaze::DynamicMatrix<int>& A,                //rosettacode
        blaze::DynamicMatrix<int>& X);                                          //rosettacode
void add_multiple_row(blaze::DynamicMatrix<int>& A, int i, int k, int v);       //rosettacode
void divide_row(blaze::DynamicMatrix<int>& A, int i, int v);                    //rosettacode
void swap_rows(blaze::DynamicMatrix<int>& A, int i, int k);                     //rosettacode
void to_reduced_row_echelon_form_without_X(blaze::DynamicMatrix<int>& A);  */   //rosettacode
void extendedEuclid(int a, int b, int *results);                                //https://comeoncodeon.wordpress.com/2011/10/09/modular-multiplicative-inverse/


//self-written functions
int up_action(int a, int b, int k);
int down_action(int a, int b, int k);

int action_matrix(int k, int *pt_upmatrix[], int *pt_downmatrix[]);
int boundary_names(int k, int n, int *pointer[]);
int erase_value(int vect[], int n, int length, int *pointer);
int fill_boundary_matrices(int k, int degree, int *pt_upmatrix[], 
        int *pt_downmatrix[],int *pt_matrix[]);

void Gaussian_with_X(int num_rows, int num_cols, int *pt_matrixA[], 
        int* pt_matrixX[]);
void Gaussian(int num_rows, int num_cols, int *pt_matrix[]);
void add_rows(int *pt_matrix[], int i, int j, int mult1, int mult2, 
        int num_rows);
void col_swap(int *pt_matrix[], int i, int j, int num_rows);
void row_swap(int *pt_matrix[], int i, int j, int num_cols);

int boundary_matrices(int k, int degree, int *pt_matrixF[], int *pt_matrixG[]);
int matrix_rank(blaze::DynamicMatrix<int>& A);
void check_for_gcd(int m, int n, int *pt_matrix[]);

void matrix_inverse(blaze::DynamicMatrix<int>& M, blaze::DynamicMatrix<int>& N);

int diag_to_smith(blaze::DynamicMatrix<int>& A);
int smith_normal_form(blaze::DynamicMatrix<int>& A);
void HNF(blaze::DynamicMatrix<int>& A);
void row_gcd(blaze::DynamicMatrix<int>& A, int i, int j, int l);

int psi(int a, int b);

bool check_diagonal(blaze::DynamicMatrix<int>& A);

void output_result(blaze::DynamicVector<int> output, int s, int l, int ones, int degree, int k);