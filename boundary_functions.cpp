#include "myheader.h"


int up_action(int a, int b, int k) {
    
    int result;
    result = 2 * b - a; // dihedral quandle
    result %= k; //make sure result is bigger than 0.
    while (result < 0){
        result += k;
    }
    return(result);
}

int down_action(int a, int b, int k){

    int result; 
    result = a; //assume quandle, not biquandle.
    return(result);
}

int action_matrix(int k, int *pt_upmatrix[], int *pt_downmatrix[]){
    //This function calculates a matrix for all the up and down actions.
    for(int i = 0; i < k; ++i){
        for(int j = 0; j < k; ++j){
            *(pt_upmatrix[i] + j) = up_action(i, j, k);
            *(pt_downmatrix[i] + j) = down_action(i, j, k);
        }
    }
    return(0);
}

int boundary_names(int k, int n, int *pointer[]){
    //This function calculates the row and column names, consisting of n-tuples.

    for(int i = 0; i < n; ++i){
        int length_vec = pow(k,i);  
        int vec[k * length_vec];    
        for(int j = 0; j < k; ++j){
            for(int l = 0; l < length_vec; ++l){
                vec[l + j * length_vec] = j;             
            }
        } 
        for(int j = 0; j < pow(k,n-1-i); ++j){ 
            for(int l = 0; l < (k * length_vec); ++l){ 
                *(pointer[l + j * k * length_vec] + (n - i - 1)) = vec[l];
            }
        }
    }
    return(0);
}

int erase_value(int *vect[], int n, int length, int *pointer,int row){
    int l = 0;
    for(int i = 0; i < length; ++i){
        if(i != n){
            *(pointer + l) = *(vect[row] + i);
            if(length==2){
                std::cout << *(vect[row] + i);
            }
            l++;
        }
    }
    
    return(0);
}

int fill_boundary_matrices(int k, int degree, int *pt_upmatrix[], int *pt_downmatrix[],int *pt_matrix[]){
    int m = pow(k, degree), n = pow(k, degree-1);
    
    int *pt_rownames[m], *pt_colnames[n];
    blaze::DynamicMatrix<int> row_names(m, degree), col_names(n, (degree - 1));
 
    for(int i = 0; i < m; ++i){
        pt_rownames[i] = &row_names(i,0);
    }
    for(int i = 0; i < n; ++i){
        pt_colnames[i] = &col_names(i,0);
    }
    
    boundary_names(k, degree, pt_rownames);
    boundary_names(k, (degree - 1), pt_colnames);
    //std::cout << row_names << "\n" << col_names << "\n\n";
    blaze::DynamicVector<int> name_vec (degree - 1), name_vec2 (degree-1);
    
    for(int i = 0; i < m; ++i){
        for(int j = 0; j < degree; ++j){
            int b = row_names(i,j);
            erase_value(pt_rownames,j,degree,&name_vec[0],i);  
            name_vec2 = name_vec;
            if(degree==2){std::cout << i << "\t" << j << "\t" << b << "\t" <<name_vec << "\n";};
            for(int l = 0; l < j; ++l){
                name_vec[l] = *(pt_upmatrix[name_vec[l]] + b);
            }
            for(int l = j; l < (degree - 1); ++l){
                name_vec[l] = *(pt_downmatrix[name_vec[l]] + b);
            }
            int row_ident = -1;
            for(int l = 0; l < n; ++l){
                if(name_vec == blaze::trans(blaze::row(col_names,l))){
                    row_ident = l;
                    break;
                }
            }
            if(row_ident >= 0){
                if(0 == j % 2){
                    *(pt_matrix[i] + row_ident) -= 1;
                } else{
                    *(pt_matrix[i] + row_ident) += 1;
                }
            } else{
                return(1);
            }
            
            row_ident = -1;
            for(int l = 0; l < n; ++l){
                if(name_vec2 == blaze::trans(blaze::row(col_names,l))){
                    row_ident = l;
                    break;
                }
            }
            if(row_ident >= 0){
                if(0 == j % 2){
                    *(pt_matrix[i] + row_ident) += 1;
                } else{
                    *(pt_matrix[i] + row_ident) -= 1;
                }
            } else{
                return(1);
            }
        }
    }
    return(0);
}

int boundary_matrices(int k, int degree, int *pt_matrixF[], int *pt_matrixG[]){
    blaze::DynamicMatrix<int> up_matrix(k, k), down_matrix(k ,k);
    int *pt_upmatrix[k], *pt_downmatrix[k];
    for(int i = 0; i < k; ++i){
        pt_upmatrix[i] = &up_matrix(i, 0);
        pt_downmatrix[i] = &down_matrix(i, 0);
    }
    action_matrix(k, pt_upmatrix, pt_downmatrix);
    fill_boundary_matrices(k, degree + 1, pt_upmatrix, pt_downmatrix, pt_matrixF);
    fill_boundary_matrices(k, degree, pt_upmatrix, pt_downmatrix, pt_matrixG);
    return(0);
}

void row_swap(int *pt_matrix[], int i, int j, int num_cols){
    for(int l = 0; l < num_cols; ++l){
        std::swap(*(pt_matrix[i] + l), *(pt_matrix[j] + l));
    }
    return;
}

void col_swap(int *pt_matrix[], int i, int j, int num_rows){
    for(int l = 0; l < num_rows; ++l){
        std::swap(*(pt_matrix[l] + i), *(pt_matrix[l] + j));
    }
    return;
}

void add_rows(int *pt_matrix[], int i, int j, int mult1, int mult2, int num_cols){
    
    for(int l = i; l < num_cols; ++l){
        *(pt_matrix[j] + l) *= mult2;
        *(pt_matrix[j] + l) -= *(pt_matrix[i] + l) * mult1;
    }
    return;
}

void Gaussian(int num_rows, int num_cols, int *pt_matrix[]){
    for(int i = 0; i < num_cols; ++i){
        bool nonzero_row = true;
        if(0 == *(pt_matrix[i] + i)){
            nonzero_row = false;
            for(int j = (i+1); j < num_rows; ++j){
                if(0 != *(pt_matrix[j] + i)){
                    row_swap(pt_matrix, i, j, num_cols);
                    nonzero_row = true;
                    break;
                }
            }
        }
        if(nonzero_row){
            for(int j = (i+1); j < num_rows; ++j){
                if(0 != *(pt_matrix[j] + i)){
                    int lcm = boost::math::lcm(*(pt_matrix[j] + i), *(pt_matrix[i] + i));
                    add_rows(pt_matrix, i, j, lcm / *(pt_matrix[i] + i), lcm / *(pt_matrix[j] + i), num_cols);
                }
            }
        }
    }
    return;
}

void Gaussian_with_X(int num_rows, int num_cols, int *pt_matrixA[], int* pt_matrixX[]){
    for(int i = 0; i < num_cols; ++i){
        bool nonzero_row = true;
        if(0 == *(pt_matrixA[i] + i)){
            nonzero_row = false;
            for(int j = (i+1); j < num_rows; ++j){
                if(0 != *(pt_matrixA[j] + i)){
                    row_swap(pt_matrixA, i, j, num_cols);
                    row_swap(pt_matrixX, i, j, num_cols);
                    nonzero_row = true;
                    break;
                }
            }
        }
        /*if(!nonzero_row){
            for(int j = (i+1); j < num_cols; ++j){
                if(0 != *(pt_matrixA[i] + j)){
                    col_swap(pt_matrixA, i, j, num_rows);
                    nonzero_row = true;
                    break;
                }
            }
        }
        */
        if(nonzero_row){
            for(int j = (i+1); j < num_rows; ++j){
                if(0 != *(pt_matrixA[j] + i)){
                    int lcm = boost::math::lcm(*(pt_matrixA[j] + i), *(pt_matrixA[i] + i));
                    add_rows(pt_matrixX, i, j, lcm / *(pt_matrixA[i] + i), lcm / *(pt_matrixA[j] + i), num_cols);
                    add_rows(pt_matrixA, i, j, lcm / *(pt_matrixA[i] + i), lcm / *(pt_matrixA[j] + i), num_cols);
                }
            }
        }
        
    }
    return;
}