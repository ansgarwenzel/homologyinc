/* 
 * File:   main.cpp
 * Author: Ansgar Wenzel
 *
 * Created on 30. April 2014, 18:10
 */

#include "myheader.h"

int main(void){
    //This program calculates the homology of a rack or birack.
    int k = 3, degree = 2, m = pow(k, degree + 1), n = pow(k, (degree)), o = pow(k, (degree - 1));
    blaze::DynamicMatrix<int> matrixF(m,n,0), matrixG(n,o,0);   //turn into sparse matrices?
    
    //This part calculates the boundary matrices.
    int *pt_matrixF[m], *pt_matrixG[n];
    
    for(int i = 0; i < m; ++i){
        pt_matrixF[i] = &matrixF(i,0);
    }
    for(int i = 0; i < n; ++i){
        pt_matrixG[i] = &matrixG(i,0);
    }
    
    boundary_matrices(k, degree, pt_matrixF, pt_matrixG);                       //calculate boundary matrices
  
    int *pt_matrixX[n];
    
    blaze::DynamicMatrix<int> X(n,n,0);
    for(int i = 0; i < n; ++i){
        X(i,i) = 1;
        pt_matrixX[i] = &X(i,0);
    }
    
    Gaussian_with_X(n, o, pt_matrixG, pt_matrixX);                              //calculate D
    
    int rho = matrix_rank(matrixG);                                             //calculate rank of matrix D
    blaze::DynamicMatrix<int> new_X(n-rho,n);
    int dummy = 0;      
    for(int i = rho; i < n; ++i){                                               //remove zero rows
        for(int l = 0; l < n; ++l){ 
            new_X(dummy,l) = X(i,l);
        }
        dummy++;
    }                               
    matrixG = new_X;
    blaze::clear(new_X);
    blaze::clear(X);
    
    
    
    Gaussian(m, n, pt_matrixF);                                                 //calculate row space of F
    check_for_gcd(m, n, pt_matrixF);
    
    matrixG = blaze::trans(matrixG);
    matrix_inverse(matrixG, matrixF); //check. not correct
    //std::cout << matrixG;
    
    smith_normal_form(matrixF, k);
    blaze::clear(matrixG);
    int length = std::min(blaze::columns(new_F),blaze::rows(new_F)), l = 0, ones = 0;
    blaze::DynamicVector<int> diagonal(length);
    for(int i = 0; i < length; ++i){
        if(1==matrixF(i,i)){
            ones++;
        } else if(0 != new_F(i,i)){
            l++;
            diagonal[i] = new_F(i,i);
        }
        else{
            break;
        }
    }
    blaze::clear(new_F);
    
    output_result(diagonal, length, l, ones, degree, k);
    
    return(0);
}

void output_result(blaze::DynamicVector<int> output, int s, int l, int ones, int degree, int k){
    char ordinal[4];
    
    switch(degree%10){
        case 1:
            std::strcpy(ordinal,"st ");
            break;
        case 2:
            std::strcpy(ordinal,"nd ");
            break;
        case 3:
            std::strcpy(ordinal,"rd ");
            break;
        default:
            std::strcpy(ordinal,"th ");
    }
    if(s > (l+ones)){
        if(l > 0){
        std::cout << "The " << degree << ordinal << "rack homology group of R_"
                << k << " is isomorphic to\nZ^" << s-(l+ones) << 
                ",\nplus the following:\n";
        } else{
            std::cout << "The " << degree << ordinal << "rack homology group of R_"
                << k << " is isomorphic to\nZ^" << s-(l+ones) << "\n";
        }
    } else {
        std::cout << "The " << degree << ordinal << "rack homology group of R_"
                << k << " is isomorphic to the following: \n";
    }
    if(l > 0){
        for(int i = 0; i < (l-1); ++i){
            std::cout << "Z_" << output[i] << " plus\t";
        }
        std::cout << "Z_" << output[l-1] << ".\n";
    }
    
    return;
}
